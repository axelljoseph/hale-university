var options = {
    useEasing : true, 
    useGrouping : false, 
    separator : ',', 
    decimal : '.', 
    prefix : '', 
	suffix : ' MILLION AED' 
};
var demo = new CountUp("#counterOne", 0, 28, 0, 3, options);
demo.start();